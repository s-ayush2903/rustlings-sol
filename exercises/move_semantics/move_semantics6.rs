// move_semantics6.rs
// Execute `rustlings hint move_semantics6` or use the `hint` watch subcommand for a hint.
// You can't change anything except adding or removing references.

fn main() {
    let data = "Rust is great!".to_string();

    get_char(&data);

    string_uppercase(data);
}

// Should not take ownership: to reference bhej do
fn get_char(data: &String) -> char {
    data.chars().last().unwrap()
}

// Should take ownership: to poora ds bhej do(mutability as per need)
fn string_uppercase(mut data: String) {
    data = data.to_uppercase();

    println!("{}", data);
}
