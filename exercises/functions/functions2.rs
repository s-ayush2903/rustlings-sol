// functions2.rs
// Execute `rustlings hint functions2` or use the `hint` watch subcommand for a hint.

fn main() {
    call_me(3);
}

fn call_me(times: u16) {
    for i in 0..times {
        println!("Called #{}th time...", i);
    }
}
