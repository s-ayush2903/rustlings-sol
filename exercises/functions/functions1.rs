// functions1.rs
// Execute `rustlings hint functions1` or use the `hint` watch subcommand for a hint.

fn call_me() -> u16 {
    println!("called from within an fn");
    return 1;
}

fn main() {
    call_me();
}
