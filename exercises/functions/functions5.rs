// functions5.rs
// Execute `rustlings hint functions5` or use the `hint` watch subcommand for a hint.

fn main() {
    println!("The square of 3 is {}", square_of(3));
}

fn square_of(num: i32) -> i32 {
    num * num
}